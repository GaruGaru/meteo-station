import com.google.gson.Gson;
import it.modularity.publisher.rabbit.MeteoMessageAdapter;
import it.modularity.serial.impl.mock.DefaultDataGenerator;
import it.modularity.station.commons.MeteoMessage;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestMessageAdapter {

    @Test
    public void testAdapterConsistency() {

        final String payload = DefaultDataGenerator.create().get();
        final String location = "42x11";
        final String name = "station-name";

        final Gson gson = new Gson();

        MeteoMessageAdapter adapter = new MeteoMessageAdapter(gson);

        MeteoMessage message = new MeteoMessage(name, location, payload);

        byte[] bytes = adapter.apply(message);

        assertThat(bytes)
                .isNotNull()
                .isNotEmpty();

        MeteoMessage decodedMessage = gson.fromJson(new String(bytes), MeteoMessage.class);

        assertThat(decodedMessage)
                .isNotNull()
                .hasNoNullFieldsOrProperties();

        assertThat(decodedMessage.getPayload()).isEqualTo(payload);
        assertThat(decodedMessage.getStationName()).isEqualTo(name);
        assertThat(decodedMessage.getStationLocation()).isEqualTo(location);

    }
}
