
import it.modularity.configuration.StationConfiguration;
import it.modularity.confy.Confy;
import it.modularity.station.commons.MeteoMessage;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestMeteoMessage {

    @Test
    public void testMeteoMessageStaticFactory() {
        StationConfiguration configuration = Confy.implement(StationConfiguration.class, "test-station-configuration");
        MeteoMessage meteoMessage = new MeteoMessage(configuration.getStationName(), configuration.getStationLocation(), "payload");
        assertThat(meteoMessage.getStationName()).isEqualTo(configuration.getStationName());
        assertThat(meteoMessage.getStationLocation()).isEqualTo(configuration.getStationLocation());
    }

}
