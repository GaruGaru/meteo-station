package commons;


import it.modularity.publisher.common.MessagePublisher;
import it.modularity.station.commons.MeteoMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestableMessagePublisher implements MessagePublisher<MeteoMessage> {

    public static TestableMessagePublisher create() {
        return new TestableMessagePublisher();
    }

    private boolean initialized;

    private boolean disposed;

    private List<MeteoMessage> publishedMessages;

    private TestableMessagePublisher() {
        this.initialized = false;
        this.disposed = false;
        this.publishedMessages = new ArrayList<>();
    }

    @Override
    public void init() {
        this.publishedMessages.clear();
        this.initialized = true;
    }

    @Override
    public void publish(MeteoMessage... messages) {
        this.publishedMessages.addAll(Arrays.asList(messages));
    }

    @Override
    public void dispose() {
        this.disposed = true;
    }

    public List<MeteoMessage> getPublishedMessages() {
        return publishedMessages;
    }

    public boolean isDisposed() {
        return disposed;
    }

    public boolean isInitialized() {
        return initialized;
    }
}
