package commons;

import it.modularity.serial.ISerial;

public class TestableSerial implements ISerial {

    public static TestableSerial withPayloads(String... payloads){
        return new TestableSerial(payloads);
    }

    private boolean connected;

    private SerialListener listener;

    private String[] messages;

    private TestableSerial(String[] messages) {
        this.messages = messages;
    }

    @Override
    public void connect() {
        this.connected = true;
        for (String message : messages)
            listener.onMessage(this, message);
    }

    @Override
    public void disconnect() {
        this.connected = false;
    }

    @Override
    public void listenOn(SerialListener listener) {
        this.listener = listener;
    }

    public boolean isConnected() {
        return connected;
    }

    public String[] getMessages() {
        return messages;
    }
}
