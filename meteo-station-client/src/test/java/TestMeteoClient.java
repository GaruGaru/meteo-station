import commons.TestableMessagePublisher;
import commons.TestableSerial;
import it.modularity.client.MeteoClient;
import it.modularity.configuration.StationConfiguration;
import it.modularity.confy.Confy;
import it.modularity.serial.impl.mock.DefaultDataGenerator;
import it.modularity.station.commons.MeteoMessage;
import org.junit.Test;

import java.util.function.Supplier;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class TestMeteoClient {


    @Test
    public void testClient() {
        Supplier<String> generator = DefaultDataGenerator.create();

        StationConfiguration configuration = Confy.implement(StationConfiguration.class, "test-station-configuration");

        TestableSerial serial = TestableSerial.withPayloads(generator.get(), generator.get(), generator.get());

        TestableMessagePublisher publisher = TestableMessagePublisher.create();

        MeteoClient client = new MeteoClient(configuration, serial, publisher);

        client.init();

        client.start();

        assertThat(serial.isConnected()).isTrue();

        assertThat(publisher.isInitialized()).isTrue();

        assertThat(publisher.getPublishedMessages()).hasSize(3);

        for (String payload : serial.getMessages()) {
            assertThat(publisher.getPublishedMessages()).contains(
                    new MeteoMessage(
                            configuration.getStationName(),
                            configuration.getStationLocation(),
                            payload
                    )
            );
        }

        client.dispose();

        assertThat(publisher.isDisposed()).isTrue();

        assertThat(serial.isConnected()).isFalse();
    }
}
