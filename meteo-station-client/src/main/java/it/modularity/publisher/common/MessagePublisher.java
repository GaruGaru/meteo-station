package it.modularity.publisher.common;

public interface MessagePublisher<M> {
    void init();
    void publish(M... messages);
    void dispose();
}
