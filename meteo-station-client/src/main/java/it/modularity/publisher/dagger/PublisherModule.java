package it.modularity.publisher.dagger;

import com.google.gson.Gson;
import dagger.Module;
import dagger.Provides;
import it.modularity.configuration.StationConfiguration;
import it.modularity.publisher.common.MessagePublisher;
import it.modularity.publisher.rabbit.MeteoMessageAdapter;
import it.modularity.publisher.rabbit.MeteoMessagePublisher;
import it.modularity.station.commons.MeteoMessage;

import javax.inject.Singleton;
import java.util.function.Function;

@Module
public class PublisherModule {

    @Provides
    @Singleton
    public Function<MeteoMessage, byte[]> provideMeteoMessageAdapter(Gson gson) {
        return new MeteoMessageAdapter(gson);
    }

    @Provides
    @Singleton
    public MessagePublisher<MeteoMessage> provideMeteoMessagePublisher(StationConfiguration configuration, Function<MeteoMessage, byte[]> adapter) {
        String[] queues = configuration.queues().split(",");
        return new MeteoMessagePublisher(
                configuration.amqpUri(),
                adapter,
                configuration.exchange(),
                queues,
                configuration.routingKey()
        );
    }
}
