package it.modularity.publisher.rabbit;


import it.modularity.station.commons.MeteoMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class MeteoMessagePublisher extends AbstractRabbitMQPublisher<MeteoMessage> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractRabbitMQPublisher.class);

    private final String exchange;
    private final String[] queues;
    private final String routingKey;

    public MeteoMessagePublisher(String amqpUri, Function<MeteoMessage, byte[]> adapter, String exchange, String[] queues, String routingKey) {
        super(amqpUri, adapter);
        this.exchange = exchange;
        this.queues = queues;
        this.routingKey = routingKey;
    }

    @Override
    protected void connect() {
        super.connect();
        logger.info("Publisher connected to: {}", getAmqpUri());
    }

    @Override
    public void init() {
        connect();
        declareQueues(exchange, queues, routingKey);
    }

    @Override
    public void publish(MeteoMessage... messages) {
        for (MeteoMessage meteoMessage : messages)
            publish(exchange, routingKey, meteoMessage);
    }

}
