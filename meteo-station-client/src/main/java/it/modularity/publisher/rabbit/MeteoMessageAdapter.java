package it.modularity.publisher.rabbit;

import com.google.gson.Gson;
import it.modularity.station.commons.MeteoMessage;

import java.util.function.Function;

public class MeteoMessageAdapter implements Function<MeteoMessage, byte[]> {

    private final Gson gson;

    public MeteoMessageAdapter(Gson gson) {
        this.gson = gson;
    }

    @Override
    public byte[] apply(MeteoMessage meteoMessage) {
        return gson.toJson(meteoMessage).getBytes();
    }

}
