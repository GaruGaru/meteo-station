package it.modularity.publisher.rabbit;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import it.modularity.publisher.common.MessagePublisher;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

public abstract class AbstractRabbitMQPublisher<M> implements MessagePublisher<M> {

    private String amqpUri;

    private Function<M, byte[]> messageAdapter;

    private Channel channel;

    AbstractRabbitMQPublisher(String amqpUri, Function<M, byte[]> adapter) {
        this.amqpUri = amqpUri;
        this.messageAdapter = adapter;
    }

    protected void connect() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUri(this.amqpUri);
            this.channel = factory.newConnection().createChannel();
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException | TimeoutException | URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    // Only one client will use the queues
    public String declareExchange(String exchangeName, String type, String routingKey) {
        try {
            channel.exchangeDeclare(exchangeName, type, true);
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, exchangeName, routingKey);
            return queueName;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    //  If several clients want to share a queues with a well-known name
    public void declareQueues(String exchangeName, String[] queues, String routingKey) {
        try {
            channel.exchangeDeclare(exchangeName, "direct", true);
            for (String queueName : queues) {
                final String name = queueName.trim();
                channel.queueDeclare(name, true, false, false, null);
                channel.queueBind(name, exchangeName, routingKey);
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void declareQueue(String exchangeName, String queue, String routingKey) {
        declareQueues(exchangeName, new String[]{queue}, routingKey);
    }


    void publish(String exchange, String routingKey, M message) {
        try {
            channel.basicPublish(exchange, routingKey, null, messageAdapter.apply(message));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void dispose() {
        try {
            this.channel.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public String getAmqpUri() {
        return amqpUri;
    }

}
