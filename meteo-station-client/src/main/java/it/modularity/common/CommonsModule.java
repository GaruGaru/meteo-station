package it.modularity.common;

import com.google.gson.Gson;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class CommonsModule {

    @Singleton
    @Provides
    public Gson provideGson() {
        return new Gson();
    }

}
