package it.modularity.serial;

import java.util.function.Consumer;

/**
 * Created by Garu on 18/02/2017.
 */
public class SerialAdapter implements ISerial.SerialListener {

    private Consumer<String> consumer;

    public SerialAdapter(Consumer<String> consumer) {
        this.consumer = consumer;
    }

    @Override
    public void onMessage(ISerial serial, String message) {
        consumer.accept(message);
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }
}
