package it.modularity.serial;

/**
 * Created by Garu on 18/02/2017.
 */
public interface ISerial {

    void connect();

    void disconnect();

    void listenOn(SerialListener listener);

    interface SerialListener {
        void onMessage(ISerial serial, String message);
        void onError(Throwable throwable);
    }

}
