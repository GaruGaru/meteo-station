package it.modularity.serial.dagger;

import dagger.Module;
import dagger.Provides;
import it.modularity.serial.ISerial;
import it.modularity.serial.impl.com.SerialCom;
import it.modularity.serial.impl.mock.SerialMock;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.concurrent.TimeUnit;

@Module
public class SerialModule {

    private static final long MOCK_SEND_DELAY = TimeUnit.SECONDS.toMillis(1);

    @Provides
    @Singleton
    public ISerial provideDefaultSerial(@Named("debug") boolean debugMode) {
        if (!debugMode) {
            return SerialCom.getDefault();
        } else {
            return new SerialMock(MOCK_SEND_DELAY);
        }
    }


}
