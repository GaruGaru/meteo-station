package it.modularity.serial.impl.mock;


import it.modularity.serial.ISerial;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

/**
 * Created by Garu on 18/02/2017.
 */
public class SerialMock implements ISerial {

    private static final Logger logger = LoggerFactory.getLogger(SerialMock.class);

    private Supplier<String> messageSupplier;

    private SerialListener listener;

    private long delay;

    public SerialMock(Supplier<String> messageSupplier, long delay) {
        this.messageSupplier = messageSupplier;
        this.delay = delay;
    }

    public SerialMock(long delay) {
        this(DefaultDataGenerator.create(), delay);
    }

    @Override
    public void connect() {
     /*   TimerTask task = new TimerTask() {
            @Override
            public void run() {
                listener.onMessage(SerialMock.this, messageSupplier.get());
            }
        };

        Timer timer = new Timer(false);
        timer.schedule(task, 0, delay);
        */


        while (true) {

            if (listener != null)
                listener.onMessage(SerialMock.this, messageSupplier.get());
            else
                logger.warn("Serial (mock) message ignored, no listener set");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

            }
        }
    }

    @Override
    public void disconnect() {

    }

    @Override
    public void listenOn(SerialListener listener) {
        this.listener = listener;
    }

    @Override
    public String toString() {
        return "SerialMock";
    }
}
