package it.modularity.serial.impl.com;


import gnu.io.CommPortIdentifier;
import it.modularity.serial.ISerial;
import it.modularity.serial.utils.RXTXSerialUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by Tommaso Garuglieri on 16/06/2016.
 */
public class SerialCom extends SerialRXTXBase implements ISerial {

    private final Logger logger = LoggerFactory.getLogger(SerialCom.class);

    private SerialListener listener;

    public static ISerial getDefault() {
        return new SerialCom(RXTXSerialUtils.getDevice(), SerialRXTXBase.DEFAULT_BAUDRATE);
    }

    public static ISerial fromPort(String port, int baudRate) {
        return new SerialCom(RXTXSerialUtils.getDeviceByPort(port), baudRate);
    }

    private SerialCom(CommPortIdentifier port, int baudRate) {
        super(port, baudRate);
    }

    @Override
    public void connect() {
        this.connectSerial();
    }

    @Override
    public void disconnect() {
        this.disconnectSerial();
    }

    @Override
    public void listenOn(SerialListener listener) {
        if (listener == null)
            throw new IllegalArgumentException("Serial listener can't be null");
        this.listener = listener;
    }

    @Override
    public void onSerialMessage(String message) {
        listener.onMessage(this, message);
    }

    @Override
    protected void onError(Throwable throwable) {
        listener.onError(throwable);
    }

    @Override
    protected void onConnected() {
        logger.info("Serial connected");
    }

    @Override
    protected void onDisconnected() {
        logger.info("Serial disconnected");
    }
}
