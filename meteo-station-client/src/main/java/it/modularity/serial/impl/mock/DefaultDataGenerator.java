package it.modularity.serial.impl.mock;

import java.util.Random;
import java.util.function.Supplier;

/**
 * Created by Garu on 22/01/2017.
 */
public class DefaultDataGenerator implements Supplier<String> {

    public static Supplier<String> create() {
        return new DefaultDataGenerator();
    }

    private DefaultDataGenerator() {
    }

    private final Random random = new Random();

    private String generate() {
        String pattern = "%f/%f/%f/%f/%f/%f/%f/%f";

        double rainQuantity = getRainQuantity();
        double rain = (rainQuantity != 0) ? 1 : 0;

        return String.format(pattern, getTemperature(), getHumidity(), getPressure(), getWindSpeed(), getWindDirection(),
                rain, rainQuantity, getLight());
    }

    private double getTemperature() {
        return random(-15, 50);
    }

    private double getHumidity() {
        return random(0, 99);
    }

    private double getPressure() {
        return random.nextDouble();
    }

    private double getWindSpeed() {
        return random(0, 10);
    }

    private double getWindDirection() {
        return random(0, 360);
    }

    private double getRainQuantity() {
        return random.nextDouble();
    }

    private double getLight() {
        return random(0, 255);
    }


    private double random(int min, int max) {
        return random.nextInt(max + 1 + min) - min;
    }

    @Override
    public String get() {
        return generate();
    }
}
