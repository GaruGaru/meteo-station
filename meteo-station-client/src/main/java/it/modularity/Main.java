package it.modularity;

import it.modularity.client.MeteoClient;
import it.modularity.client.dagger.Injector;

import javax.inject.Inject;

public class Main {

    public static Main create() {
        return new Main();
    }

    @Inject
    MeteoClient client;

    private Main() {
        Injector.inject(this);
    }

    public void execute() {
        this.client.init();
        this.client.start();
    }

    public static void main(String[] args) {
        Main.create().execute();
    }

}
