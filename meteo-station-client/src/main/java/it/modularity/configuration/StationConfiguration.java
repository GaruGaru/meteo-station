package it.modularity.configuration;

import it.modularity.confy.annotations.Param;

public interface StationConfiguration {

    @Param.String(key = "station.name", defaultValue = "station")
    String getStationName();

    @Param.String(key = "station.platform", defaultValue = "unknown")
    String getStationPlatform();

    @Param.String(key = "station.location", defaultValue = "0x0")
    String getStationLocation();

    @Param.String(key = "amqp.uri")
    String amqpUri();

    @Param.String(key = "amqp.routingkey", defaultValue = "meteo-exchange")
    String routingKey();

    @Param.String(key = "amqp.exchange", defaultValue = "meteo-exchange")
    String exchange();

    @Param.String(key = "amqp.queues", defaultValue = "meteo-queues")
    String queues();

    @Param.Boolean(key = "station.debug")
    boolean debug();

}
