package it.modularity.configuration.dagger;

import dagger.Module;
import dagger.Provides;
import it.modularity.configuration.StationConfiguration;
import it.modularity.confy.Confy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.stream.Collectors;

@Module
public class ConfigurationModule {

    private static final Logger logger = LoggerFactory.getLogger(ConfigurationModule.class);

    @Singleton
    @Provides
    public StationConfiguration provideConfiguration() {
        Confy conf = Confy.create("station-configuration", true);

        logger.info("Configuration:\n\t{}\n",
                conf.toMap().entrySet()
                        .stream().map(e -> e.getKey() + ": " + e.getValue())
                        .collect(Collectors.joining("\n\t"))
        );

        return Confy.implement(StationConfiguration.class, conf);
    }

    @Named("debug")
    @Provides
    public boolean provideDebugMode(StationConfiguration configuration) {
        return configuration.debug();
    }

}
