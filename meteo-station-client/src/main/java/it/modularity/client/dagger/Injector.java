package it.modularity.client.dagger;

import it.modularity.Main;

public class Injector {

    public static void inject(Main main) {
        DaggerMeteoClientComponent.builder()
                .build()
                .inject(main);
    }

}
