package it.modularity.client.dagger;


import dagger.Component;
import it.modularity.Main;
import it.modularity.common.CommonsModule;
import it.modularity.configuration.dagger.ConfigurationModule;
import it.modularity.publisher.dagger.PublisherModule;
import it.modularity.serial.dagger.SerialModule;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        ConfigurationModule.class,
        PublisherModule.class,
        CommonsModule.class,
        SerialModule.class,
        MeteoClientModule.class
})
public interface MeteoClientComponent {
    void inject(Main main);
}
