package it.modularity.client.dagger;

import dagger.Module;
import dagger.Provides;
import it.modularity.client.MeteoClient;
import it.modularity.configuration.StationConfiguration;
import it.modularity.publisher.common.MessagePublisher;
import it.modularity.serial.ISerial;
import it.modularity.station.commons.MeteoMessage;

import javax.inject.Singleton;

@Module
public class MeteoClientModule {

    @Singleton
    @Provides
    public MeteoClient provideMeteoClient(StationConfiguration configuration, ISerial serial, MessagePublisher<MeteoMessage> publisher) {
        return new MeteoClient(configuration, serial, publisher);
    }

}
