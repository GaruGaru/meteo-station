package it.modularity.client;

import it.modularity.configuration.StationConfiguration;
import it.modularity.publisher.common.MessagePublisher;
import it.modularity.serial.ISerial;
import it.modularity.serial.utils.RXTXSerialUtils;
import it.modularity.station.commons.MeteoMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class MeteoClient implements ISerial.SerialListener {

    private static final Logger logger = LoggerFactory.getLogger(MeteoClient.class);

    private StationConfiguration configuration;

    private ISerial serial;

    private MessagePublisher<MeteoMessage> publisher;

    @Inject
    public MeteoClient(StationConfiguration configuration, ISerial serial, MessagePublisher<MeteoMessage> publisher) {
        this.configuration = configuration;
        this.serial = serial;
        this.publisher = publisher;
    }


    public void init() {

        if (configuration.getStationPlatform().equals("pi"))
            RXTXSerialUtils.raspberryWorkaround();

        publisher.init();

    }

    public void start() {
        serial.listenOn(this);
        serial.connect();
    }

    @Override
    public void onMessage(ISerial serial, String serialMessage) {
        MeteoMessage meteoMessage = new MeteoMessage(configuration.getStationName(), configuration.getStationLocation(), serialMessage);
        publisher.publish(meteoMessage);
        logger.info("Published: {}", meteoMessage);
    }

    @Override
    public void onError(Throwable throwable) {
        logger.error("Serial error", throwable);
    }


    public void dispose() {
        this.serial.disconnect();
        this.publisher.dispose();
    }

}
