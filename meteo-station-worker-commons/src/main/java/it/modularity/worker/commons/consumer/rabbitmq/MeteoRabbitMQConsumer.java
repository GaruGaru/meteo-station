package it.modularity.worker.commons.consumer.rabbitmq;


import it.modularity.station.commons.MeteoMessage;
import it.modularity.worker.commons.consumer.MeteoMessageAdapter;

public class MeteoRabbitMQConsumer extends AbstractRabbitMQConsumer<MeteoMessage> {

    public MeteoRabbitMQConsumer(String amqpUri, String queue) {
        super(new MeteoMessageAdapter(), amqpUri, queue);
    }

    public MeteoRabbitMQConsumer(RabbitConfiguration configuration) {
        super(new MeteoMessageAdapter(), configuration.getHost(), configuration.queue());
    }

}
