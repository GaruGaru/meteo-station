package it.modularity.worker.commons.consumer.rabbitmq;

import it.modularity.confy.annotations.Param;

public interface RabbitConfiguration {

    @Param.String(key = "amqp.host")
    String getHost();

    @Param.String(key = "amqp.queue")
    String queue();

}
