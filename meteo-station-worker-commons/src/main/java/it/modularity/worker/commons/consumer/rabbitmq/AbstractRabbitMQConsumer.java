package it.modularity.worker.commons.consumer.rabbitmq;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;

public class AbstractRabbitMQConsumer<T> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractRabbitMQConsumer.class);

    private Function<byte[], T> adapter;

    private String amqpUri;

    private String queue;

    private Connection connection;

    private Channel channel;

    public AbstractRabbitMQConsumer(Function<byte[], T> adapter, String amqpUri, String queue) {
        this.adapter = adapter;
        this.amqpUri = amqpUri;
        this.queue = queue;
    }

    public void init() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUri(this.amqpUri);
            this.connection = factory.newConnection();
            this.channel = this.connection.createChannel();
            logger.info("Connected to: {}", amqpUri);
        } catch (NoSuchAlgorithmException | KeyManagementException | URISyntaxException | TimeoutException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void deliver(Consumer<T> consumer) {
        boolean autoAck = false;
        try {
            channel.basicConsume(queue, autoAck, "consumer",
                    new DefaultConsumer(channel) {
                        @Override
                        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                                throws IOException {
                            long deliveryTag = envelope.getDeliveryTag();

                            consumer.accept(adapter.apply(body));

                            channel.basicAck(deliveryTag, false);
                        }
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public void dispose() {
        try {
            this.channel.close();
            this.connection.close();
        } catch (Exception ignored) {
        }
    }

}
