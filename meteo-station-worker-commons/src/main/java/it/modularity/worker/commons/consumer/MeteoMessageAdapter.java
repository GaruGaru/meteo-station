package it.modularity.worker.commons.consumer;

import com.google.gson.Gson;
import it.modularity.station.commons.MeteoMessage;

import java.util.function.Function;

public class MeteoMessageAdapter implements Function<byte[], MeteoMessage> {

    private final Gson gson = new Gson();

    @Override
    public MeteoMessage apply(byte[] bytes) {
        return gson.fromJson(new String(bytes), MeteoMessage.class);
    }

}
