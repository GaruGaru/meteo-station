package meteo.worker.influxdb;

import it.modularity.confy.annotations.Param;
import it.modularity.worker.commons.consumer.rabbitmq.RabbitConfiguration;

public interface InfluxWorkerConfiguration extends RabbitConfiguration {

    @Param.String(key = "influx.host", defaultValue = "http://localhost:8086")
    String influxHost();

    @Param.String(key = "influx.username", defaultValue = "root")
    String influxUsername();

    @Param.String(key = "influx.password", defaultValue = "root")
    String influxPassword();

    @Param.String(key = "influx.db", defaultValue = "meteo")
    String influxDb();

}
