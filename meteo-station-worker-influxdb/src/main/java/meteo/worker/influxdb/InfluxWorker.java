package meteo.worker.influxdb;

import it.modularity.station.commons.MeteoMessage;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class InfluxWorker implements Consumer<MeteoMessage> {

    private static final Logger logger = LoggerFactory.getLogger(InfluxWorker.class);

    private InfluxDB influx;
    private final String dbName;

    public InfluxWorker(InfluxWorkerConfiguration configuration) {
        this.influx = InfluxDBFactory.connect(configuration.influxHost(), configuration.influxUsername(), configuration.influxPassword());
        this.dbName = configuration.influxDb();
    }


    public void init() {
        influx.createDatabase(dbName);
        influx.setDatabase(dbName);
        influx.setRetentionPolicy("autogen");
        logger.info("Connected to influx: " + dbName);
    }

    @Override
    public void accept(MeteoMessage message) {
        logger.info(message.toString());

        Point.Builder point = Point.measurement("stations")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS); // TODO Send correct not current timestamp

        for (Map.Entry<String, Float> data : message.getValues().entrySet())
            point.addField(data.getKey(), data.getValue())
                    .tag("station", message.getStationName())
                    .tag("location", message.getStationLocation());

        influx.write(point.build());

    }

}
