package meteo.worker.influxdb;

import it.modularity.confy.Confy;
import it.modularity.worker.commons.consumer.rabbitmq.MeteoRabbitMQConsumer;

public class Main {

    public static void main(String[] args) {

        InfluxWorkerConfiguration conf = Confy.implement(InfluxWorkerConfiguration.class, "influx-worker");

        MeteoRabbitMQConsumer consumer = new MeteoRabbitMQConsumer(conf);
        InfluxWorker worker = new InfluxWorker(conf);

        consumer.init();
        worker.init();

        consumer.deliver(worker);

    }
}
