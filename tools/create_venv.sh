#!/usr/bin/env bash

echo "Setting up virtual env for meteo-station project"

mkvirtualenv meteo-station
workon meteo-station
pip install -r requirements

echo "Done."
