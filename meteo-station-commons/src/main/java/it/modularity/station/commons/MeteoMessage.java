package it.modularity.station.commons;

import java.util.HashMap;
import java.util.Map;

public class MeteoMessage {

    private String stationName;

    private String stationLocation;

    private String payload;

    public MeteoMessage(String stationName, String stationLocation, String payload) {
        this.stationName = stationName;
        this.stationLocation = stationLocation;
        this.payload = payload;
    }


    private static final String TEMPERATURE = "temperature";
    private static final String HUMIDITY = "humidity";
    private static final String PRESSURE = "pressure";
    private static final String WIND_SPEED = "wind_speed";
    private static final String WIND_DIRECTION = "wind_direction";
    private static final String RAIN = "rain";
    private static final String RAIN_QUANTITY = "rain_quantity";
    private static final String LIGHT = "light";


    public Map<String, Float> getValues() {
        final String[] valuesNames = new String[]{
                TEMPERATURE, HUMIDITY, PRESSURE,
                WIND_SPEED, WIND_DIRECTION, RAIN,
                RAIN_QUANTITY, LIGHT
        };

        Map<String, Float> valuesMap = new HashMap<>();
        String[] values = this.payload.split("/");
        for (int i = 0; i < values.length; i++) {
            float value = Float.parseFloat(values[i].replace(",", "."));
            valuesMap.put(valuesNames[i], value);
        }
        return valuesMap;
    }


    public String getPayload() {
        return payload;
    }

    public String getStationName() {
        return stationName;
    }

    public String getStationLocation() {
        return stationLocation;
    }

    @Override
    public String toString() {
        return "MeteoMessage{" +
                "stationName='" + stationName + '\'' +
                ", payload='" + payload + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MeteoMessage that = (MeteoMessage) o;

        if (stationName != null ? !stationName.equals(that.stationName) : that.stationName != null) return false;
        if (stationLocation != null ? !stationLocation.equals(that.stationLocation) : that.stationLocation != null)
            return false;

        return payload != null ? payload.equals(that.payload) : that.payload == null;
    }

    @Override
    public int hashCode() {
        int result = stationName != null ? stationName.hashCode() : 0;
        result = 31 * result + (stationLocation != null ? stationLocation.hashCode() : 0);
        result = 31 * result + (payload != null ? payload.hashCode() : 0);
        return result;
    }
}
