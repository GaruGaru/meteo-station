# Meteo station 


This is a standard maven project with client and worker submodules.
dependencies and build configurations 
are defined in the [pom files](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html).

The executable jar is built using the maven-shade plugin that includes pom defined dependencies in the jar.

Class instances are provided by Dagger 2 dependency injection that uses package based multiple modules.

Logging is managed by Logback (configuration in logback.xml)


## Prepare for local run

Build and test project with submodules 

    mvn clean package 

Start local rabbitmq instance with docker (or install manually if you don't have docker)

    docker-compose -f docker/docker-compose.yml

### Client


Run client (publisher) shaded jar

    java -jar meteo-station-client/target/meteo-station-client-1.0-SNAPSHOT.jar 
    

### Worker


Run worker (consumer) shaded jar

    java -jar meteo-station-worker/target/meteo-station-worker-1.0-SNAPSHOT.jar  